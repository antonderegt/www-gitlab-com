---
layout: handbook-page-toc
title: "Career Development Conversation"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Why career development for team members is key to being a great leader

Providing team members with a clear path of advancement within the group, department or division that includes career development opportunities is a win-win-win for team members, managers and GitLab.  Team members who are challenged, engaged and who are actively contributing relevant solutions stay with organizations. An engaged workforce allows the department and divisions to meet their current and future needs for talent and skills and increases performance and decreases regrettable attrition.

Please make sure as a manager you have reviewed the [Career Development Section in the handbook](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/)

There are many reasons that career development conversations are important. Below are a few examples though there are many more reasons.

## Increase Team Productivity and Performance

For managers, having career development conversations offers them a chance to help develop team members which then has an impact on the overall organization.  It's a critical skill for those serious about increasing their teams' productivity and performance.  Development conversations will help the group, department, division and GitLab retain great team members, which at the end of the day also contributes to the manager's own success.

## Build a Bench of High Contributors

On top of increasing performance, these career development conversations allow managers to build a bench of top talent contributor and create a succession pipeline.  This will help reduce worries about losing top talent and who the team can turn to for critical tasks.  Good managers know that if you have a strong bench, you have a lot less to worry about.

## Be a Great Leader

When it comes down to, it the best managers or leaders - the ones that team members want to have and are happy continue being part of the team.  These are the managers that take an interest in their team members lives and career.  By learning aobut what your team members wants, what they strive for and by being able to help them drive to their goals, you will build credibility, trust and respect.  This in turns also contributes to your overall success as a manager.

## Attract, Engage and Retain Top Talent

A managers's job is not to just drive to OKRs and performance.  It is also to attract, engage and retain top talent team members and maximize their contribution to the group, department or division.  If you are interested in your team members aspirations and success, you will attract top talent because people will want to work for you.  Once you get great team members on board, you will need a plan to get them and keep them engaged. Career development can be and should be one of the ways you as a manager invest your time and energy to help drive engagement, performance and results.

# Preparing for a team member's performance and development conversation

## Career conversations do not have to be intimidating

Career development conversations are vital for the team members, managers and GitLab's success.  However, not all managers are prepared to address career development due to either lack of experience in having these conversations or a fear that the conversation will be difficult or unpleasant.  Here are a few other reasons that managers may avoid having these conversations:

*  Career development conversations will take up time they do not have and create more work
*  Team members will want different roles or opportunities on an unrealistic timeframe
*  Team members will have aspirations that do not match their current skill level
*  Team members will take conversations about their career as a promise of a promotion
*  Team members will leave after they have developed new skills

Not discounting these concerns, but as a manager if is your job to engage team members in meaningful career development conversations.  

If a team members expectations or timeframe is unrealistic, you can explore having them use their time to find opportunities that will engage and prepare them for when they opportunities arise.  If they are ready for promotion but the opportunity is not in scope or plan for the business it is best to be transparent with the team member. Discuss any timeline of if/when a position may be available or if there is no plan currently to add that role.  You can also work with the team member to look for internal opportunities like internship or cross collaborative projects to help keep the team member engaged while continuing in their role.  

If a team members expresses career ambitions that are beyond their current skill set, then that is an opportunity to talk about the next level skills and experience that might help prepar them to get them where they want to go.  If your department or division has job matrices or compentencies this is a good time to review those with the team member.  Techincal skills are only one part of the team members ability to continue to grow and succeed.  If there are issues with attitude and behaviors this is a good time to also share what the team member needs to work on in regards soft skills as part of their career growth and development.

If you are concerned that your career development conversation could be construed as a promise of a promotion, focus the conversation around skills gap development and opportunities.  Try to focus on the the things needed now rather than focusing on a specific time frame.

It is possible that some team members may leave for other opportunities after they acquire a new skill set.  However, it is even more possible that they will leave if they believe you or GitLab is not interested in their career aspirations or development.  Engaging in meaningful career development conversations show that you as a manager care about them personally and their future development. 

## Preparing for the discussion
### Thing about the team members strengths and wins

As a manager part of your job is to learn more about the team members you manage.  What are their skills, interests and career goals.  The tone of the career development conversation should be transparent, encouraging, curious and show that you are interested and invested in the team member.  Spend some time before you have a conversation to think about the following:

* The team members key strengths, skills and wins
* The team members engagement level
* What are opportunities for their development
* Current level of skills in technical or funcational areas
* What are their strong leadership compentencies or behaviors

Career development conversations shouldn't be done on a whim or just "winging it", but instead these should be thought out and conversations that you prepare for ahead of time.  You should come prepare to provide to provide to offer team members feedback, offer suggestions and to ask pertinent questions.

# What questions might team members ask

Team members want feedback on their strenghts, performance and their potential within GitLab.  Be prepared to answer the following questions:

* What you say are my top strengths or top skill?
* What skills should I build on?
* Are you aware of any development opportunities that would be helpful for me?
* I see this as my career path, what career path do you see based on my skills?
* Should I be gathering career development feedback from others?

Since you are already having regular 1:1s with your team member you may have ideas regarding your team members development aspirations.  Plan ahead and take the time to think about what questions they may ask and come prepared to the conversation.  

# Having the Discussion

## Maintain the right frame of mind

Coming to the meeting with an open mind and the willingness to learn more about the team members career development aspirations will set the right tone for the conversation.

A best practice could be having the team member start the meeting by expressing their goals for the conversation. Your job as a manager is to listen and understand their desires and help them explore options that may be available for reaching those goals.  Try and refrain from interjecting, let the team member finish before you start talking.  Try not to be judgemental on what they say, different people will have different career goals and it is important to respect their ideas.  However, this can also be a time fo ryou to provide them with feedback, suggestions, recommendations and guidance.  If possible it can also be an opportunity for you to connect them with different people within GitLab and additional resource that support their identified career path.

As a manager you should follow up on the goals and activities you both identify. This will show the team member that you do listen and have a vested interest in their future success. Also, career conversations should not be a one-time annual conversation.  A best practice is to meet at least quarterly for a check in.  Though keep in mind some team members may want to meet more often and some less frequently.


## Be open about your own career goals and development

If asked share your career goals with your team members.  This transparency shows that you are not only willing to hear their career goasl, but you are also willing to share your own career path.  That could also include discussing with your team members the areas that you have identified to focus on in the future. Be open about questions regarding your own career path and experiences to date.  Your team member may be able to take away some valuable lessons that you have learned in your own journey.

There are really great article regarding career development conversations here are a few for your review:
[5 business reasons to put employee career development at the top of your agenda](https://cezannehr.com/hr-blog/2019/03/five-reasons-to-prioritise-development/)
[Career Development Mentoring Benefits](https://www.insala.com/blog/benefits-of-career-development-mentoring)
[Why Employee Development is Important, Neglected and can Cost You Talent](https://www.forbes.com/sites/victorlipman/2013/01/29/why-development-planning-is-important-neglected-and-can-cost-you-young-talent/#22df1a8a6f63)
[If Your're Not Helping People Develop, You're Not Management Material](https://hbr.org/2014/01/if-youre-not-helping-people-develop-youre-not-management-material)
