---
layout: handbook-page-toc
title: "Compensation Review"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Annual Compensation - Communication Guidelines

### Timelines

**For Employees:**
* **Feb 1** - Managers will receive approval from Compensation to start communicating
increases on or before Feb 1.  Do not communicate to team members until we have approval from Compensation.
* **Feb 1** - Increases are effective Feb 1 and will be paid in the regular payroll cycle based on your location.
* **Feb 1 - 13**:  Managers will log into BambooHR (or Compaas) to see the final approved amount.  The final amount may be higher or lower than originally scheduled.  Make sure you understand the reason for the amount prior to communicating.  
* **Feb 14**:  Employees will be able to see their increase in BambooHR

**For Contractors:**
* **Feb 1** - Managers will receive approval from Compensation to start communicating
increases on or before Feb 1.  Do not communicate to team members until we have approval from Compensation.
* **Feb 1** - Increases are effective Feb 1 and will be paid in the regular payroll cycle based on your location.
* **Feb 1 - 6**:  Managers will log into BambooHR (or Compaas) to see the final approved amount.  The final amount may be higher or lower than originally scheduled.  Make sure you understand the reason for the amount prior to communicating.  
* **Feb 7**:  Contractors will be able to see their increase in BambooHR in order to submit invoices by Feb 8th.

### Communication Recommendations

* Communicate the increase face to face over Zoom. As a manager, this is an opportunity for you to have a conversation with your team member about their increase and the reason behind it.  Having the conversation over Zoom allows for you to have a dialogue with your team member (vs. just sharing the number or %) and allows you to pick up other information like tone and non-verbal cues which can tell you more about how someone is feeling.

* Prepare for the call ahead of time. As a manager, you should have awareness of the following facts about GitLab’s compensation principles (please review the handbook’s [Global Compensation ](/handbook/total-rewards/compensation/) page) and personal details about your team member in BambooHR:
    * Hire date
    * Current compensation (including base salary, variable (where applicable), etc.)
    * Date of last compensation adjustment
    * [Compa group](/handbook/total-rewards/compensation/compensation-calculator/#determining) (And any recent changes to compa group)
    * Location factor changes
    * Benchmark changes
    * % Increase: Please calculate this number from BambooHR to at least the hundredth place rather than using the rounded version available in Compaas, `((FY21 Salary-FY20 Salary)/FY20 Salary)`

* Communicate the adjustment at the beginning of the meeting. You want to give the team member time to ask questions and discuss their compensation adjustment. Avoid trying to rush to communicate at the end of a 1:1 meeting.

* Protect the confidentiality of other team members by avoiding saying things like “everyone else received less than you” or “you were the only team member to get a discretionary increase.”

* Avoid blaming others (For example: “I would have given you more but mgmt didn’t approve”)

* Avoid making future promises (For example: “In the next review, I will get you a large adjustment”)

### Scenarios (Situations and Reactions)

**4 Situations:**

1. **Receiving an Increase:**  A team member is receiving an adjustment based on COLA AND/OR location factor AND/OR benchmark changes AND/OR Discretionary:
    * Sample Script:
    * I am so pleased to inform you that you will be getting an adjustment in this year’s comp review. Your increase is based on one or more the following factors (COLA, Location Factor, Benchmark change and Discretionary.  
    * Your salary will be increased to (insert $ amt in local currency) which is a (insert %) adjustment effective, February 1st.
    * Your adjustment brings you to range for a (insert role) at (insert compa group).
    * Thank you for your hard work and I look forward to continuing to work with you! Do you have any questions?

2. **Not receiving an increase:**   A team member is not receiving an adjustment:
    * Sample Script:
    * As you know, GitLab just went through our annual compensation review. I wanted to inform you that you are not getting a compensation adjustment at this time. This is due to (examples below)...
        * A team member is new to the team and at the range for your aligned role and compa group.  Your performance is good, however, we hire new team members at market rate for compensation so we feel that you are compensated accurately for your role at this time. You may be eligible to participate in the [catch up comp review process](/handbook/total-rewards/compensation/compensation-review-cycle/#catch-up-compensation-review).
        * A team member is not receiving because they are above range for their role and compa group.
        * A team member needs to improve performance. For questions on specific situations, please work with your People Business Partner.
    * Although informing a team member that they are not getting a compensation adjustment is a tough message to deliver, we are highly recommending that managers have this direct conversation. This is directly aligned with our transparency value. We want everyone to know why they may not have received an adjustment and give them the space to ask questions.

3.  **Director Increases:**  
    * Director increases were handled as a separate process within the compensation review. For directors, their increases were determined by the following factors: compa group, location factor and benchmark changes like all other roles but this year, the recommended variable (as a % of salary) was increased from 10% to 15%. For more information on the shift to the split, please refer to the handbook.
    * Any Directors who have questions about their OTE and bonus plan should reach out directly to total-rewards@gitlab.com

4.  **Other scenarios:** If you have a scenario different from the above, and/or you need help with messaging, please work with your manager or [People Business Partner](/handbook/people-group/how-to-reach-us).  

**Possible Reactions:**

1. A team member is not happy with their adjustment.
    * Listen to your team member’s feedback and allow them to express their concerns.
    * Ask open ended questions to allow for the team member to share more (like what were your expectations for an adjustment this year? Why were those your expectations?)
    * Reiterate GitLab’s compensation philosophy. We pay to market for your knowledge, skills and abilities and determine the market range using various factors like location factors, benchmarks and your compa group.
    * Point your team member to the Compensation Calculator where they can see the range for their role and compa group.
    * If your team member is in good standing from a performance perspective, work with them to put together a development plan to help them achieve their goals whether it be a different compa group through skill development or a promotion.

2. A team member is not happy with their compa group.
    * Listen to your team member’s feedback and allow them to express their concerns.
    * Refer back to the conversation you had with your team member at the time compa groups were determined and reiterate the justification and feedback that you provided at that time.
    * If your team member is in good standing from a performance perspective, work with them to put together a [development plan](/handbook/people-group/learning-and-development/career-development/) to help them to move to the next compa group.

**If you need assistance after reviewing the [handbook](/handbook/total-rewards/compensation/), please work directly with your manager or your [People Business Partner](/handbook/people-group/how-to-reach-us)**
