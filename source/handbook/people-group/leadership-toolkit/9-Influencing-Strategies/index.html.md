---
layout: handbook-page-toc
title: "9 Influencing Strategies"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# 9 Influencing Strategies

The ability to influence is an essential leadership skill.  To influence is to have an impact on the behaviors, attitudes, opinions and choices of your team members and others across GitLab and externally.  Influence should not be confused with power or control.  It is also not about manipulating others to get your own way.  It is about noticing what motivates team members commitment and using that knowledge to leverage performance and results.  

Leadership has sometimes been described as the ability to influence others.  An effective leader does not move team members into action by coercion. An effective leaders will articulate the overall vision and goals for the organization.  By doing such this can motivate and move team members to action by tapping into their desires and need for success. Positive influence that is properly channeled can also bring about transformation and change for team members, department, division and the company.   A leader that exhibits and exerts positive influence in others will build trust and become a true driving force towards transparency, iteration, collaboration and results.

There are many different different influencing strategies and in this section we are going to review 9 that leaders can review.  Each strategy below will include a definition, example and ways for leaders to develop this strategy.
 

1.  Empowerment: Making others feel valued by involving them in decision making and giving them recognition. 
    * Example: Manager invites their team members to a meeting in order to take their inputs on how to improve product quality.
    * Develop: Finding win-win situations, not taking people for granted, not providing unsolicited advice to people and having confidence in the ability of others.
1.  Interpersonal Awareness: Being able to recognize and address the concerns of key stakeholders.
    * Example: A manager/leaders offers support to team members by identifying a list of questions that may be asked in meetings with senior leadership.
    * Develop: Awareness of verbal and non verbal cues, putting yourself in another person's shoes and testing your own understanding of the message.
1.  Negotiating: Gaing support from others by mutually agreeing on a common set of goals or outcomes.
    * Example: A vendor who is trying to land a big deal may offer their client incentives or a discount provided the size of the order large enough.
    * Develop: Understand the other person's requirements, identify your non-negotiable, role-play wiht a colleague, practive in a low risk real life situation.
1.  Networking: Establish and maint a network of contact who you may need to influence going forward.
    * Example: A manager spends time with their peers or cross functional partners in different activites (either work or non work related) in order to develop a better bond/relationship with their network.
    * Develop: Take interest in other peoples lives, find common ground, leverage existing relationships and take advantage of existing opportunities.
1.  Stakeholder awareness: Identifying the key stakeholder and the ability to gain support when needed.
    * Example: A manager identifies the key stakeholder and decision maker within a supporting organization and regularly meets with them to collaborate.
    * Develop: Keep your ear to the ground to understand what is happening around you, learn by example, identify the decision makers, appreciate other's points of view.
1.  Shared Vision: Showing others how your ideas support the broader organizational vision.
    * Example: A manager communicates their vision to their team by helping them understand what they can become and the steps needed to achieve the goal.
    * Develop: Always keep the outcomes in sight, speak about the benefits of the approach and get others to participate.
1.  Impact and Influence: Choosing the most appropriate time and manner to present your point, always keeping in mind your audience.
    * Example: A manager understands the landscape or demographics of their audience and is thoughtful in their presentation approach.
    * Develop: Study prominent influencers, try different approaches each time, take formal training, model good influencers within the organization.
1.  Analytical reasoning: Using analytical reasoning to convince others about your point of view.
    * Example: A team member uses data and market insights to convince their manager about their ideas.
    * Develop: Have more than 1 reason for your idea, clearly lay out the pros and cons, show clarity of thought and structure, spend time on research and case studies. 
1.  Coercion: Using threats or pressure tactics to get other to agree to your point or to comply to rules.
    *  Example: A manager makes a decision with no or little input from team and informs team members of actions or consequences if they do not comply. 
    *  Develop: Use it sparingly and not as the first resort, leave no room for ambiguity and do not use as a way to surpress your team member or others.

Here are a few additional sources regarding influencing and leadership for you to review:
[What Great Leaders Know about Influence](https://www.forbes.com/sites/rebeccanewton/2016/07/27/six-steps-to-increase-your-influence/#5609705a1edd)
[Influencing Skills: A Key to Leadership Success!](https://www.linkedin.com/pulse/influencing-skills-key-leadership-success-marcia-zidle-ms-bcc/)
[Influencing Others: A Key Leadership Skill](https://www.ginaabudi.com/influencing-others-a-key-leadership-skill/)
[The 5 Key Skills of Influential Leaders Within Every Organization](https://www.inspirationaldevelopment.com/5-key-skills-influential-leaders-every-organisation/)
[Influence and Leadership](http://www.theelementsofpower.com/power-and-influence-blog/influence-and-leadership/)
[5 Leadership Strategies Proven to Improve Performance on Your Team](https://crestcomleadership.com/2016/12/01/5-leadership-strategies-to-improve-performance-in-your-company/)
[The 7 Best Books to Improve Influencing Skills](https://www.roffeypark.com/influencing-skills/the-7-best-books-to-improve-influencing-skills/)
[7 Ways to Build Influence in the Workplace](https://www.inc.com/jayson-demers/7-ways-to-build-influence-in-the-workplace.html)
 