
---
layout: handbook-page-toc
title: "Referral Ops”
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Referral Ops

This page is for information regarding the “backend” of the [Referral Process](https://about.gitlab.com/handbook/hiring/referral-process/).

#### How to Respond to Referral Update Requests

The Recruiting Team uses the [GitLab Service Desk](/product/service-desk/) to track incoming emails to the `referral@domain` email. These emails will, in turn, show up as Issues.

To set-up notifications for the [Referral Service Desk Project](https://gitlab.com/gl-recruiting/referrals), please do the following:
1. Click on the `Bell` icon in the upper right corner (next to `Star` and `Clone`)
1. Go to `Custom Settings`
1. Click `New Issue`
1. Close the window

To take action on `Issues` in that project:
1. Click `Issues` in the left menu bar.
    * This is where incoming `referral@` emails will be; any open Issues here will need to be addressed.
1. Click on the new `Issue`.
1. Assign it to yourself (upper right corner).
1. Read the Issue.
1. Add the appropriate label(s).
1. Look-up the candidate in Greenhouse, if applicable.
1. Respond to the Issue by commenting and please be sure to do so just as you would with any other GitLab communication.
    * e.g. *“Hi, NAME. Thank you for reaching out about the status of your referral. Per our [SLA](https://about.gitlab.com/handbook/hiring/greenhouse/#making-a-referral), please allow us 10-days to review the submission. @RECRUITER is responsible for this role and they’ll provide the candidate with an update soon.”*
   * e.g. *”Thank you for the referral. @RECRUITER is responsible for this role and will provide you with an update soon.”*
1. Please be sure to `@-mention` the responsible Recruiter so that they’re aware an update is being requested.
1. If one comment addresses the entirety of the message, comment and close the Issue. If further information is needed, comment and leave the Issue open.
1. The Recruiter will reassign the Issue to themselves once they pickup the communication. They may also add any applicable labels.
1. The Assignee will close the Issue when communication is complete.

#### Referral Roundup Sessions

Our Recruiting Team occasionally organizes Referral Roundup Sessions.

The objective of theses sessions are to gather referrals, region-specific information [about where you’re based], and teach you skills related to sourcing and Greenhouse.

We’ll invite Team Members who are based in areas with a location factor *less than or equal to* **.58** to theses sessions.

Each session will have a corresponding Issue linked and in that, we ask that you please add information regarding appropriate companies to source from, local meet-up groups, conferences, job boards to advertise on, and any other information you believe will be beneficial to our recruiting efforts in your area.

A Recruiting Team Member will attend these sessions, so they be able to address any questions that arise on sourcing, LinkedIn, or Greenhouse.
