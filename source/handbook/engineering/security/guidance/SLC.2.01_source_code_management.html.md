---
layout: handbook-page-toc
title: "SLC.2.01 - Source Code Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SLC.2.01 - Source Code Management

## Control Statement
Source code is managed with GitLab-approved version control mechanisms.
 
## Context

GitLab-approved version control mechanisms should be enforced over all source code that is maintained by GitLab to ensure tracability and auditability between source code changes. Processes in place to enforce version control demonstrate strong security and SDLC maturity.
 
## Scope
This control is applicable to all areas where GitLab manages it's own source code. Note that the scope does not extend to third party systems because GitLab does not own the related source code and therefore does not have the ability to enforce version control mechanisms

## Ownership
TBD
 
## Guidance
TBD
 
## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Source Code Management control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/889) . 
 
### Policy Reference
TBD
 
## Framework Mapping
* SOC2 CC
   * CC2.1
