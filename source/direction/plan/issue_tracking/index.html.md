---
layout: markdown_page
title: "Category Direction - Issue Tracking"
---

Last reviewed: 2020-03-24

- TOC
{:toc}

## 📝 Issue Tracking

|          |                                  |
| -------- | -------------------------------- |
| Stage    | [Plan](/direction/plan/)         |
| Maturity | [Complete](/direction/maturity/) |

### Introduction and how you can help

<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

<!--
<EXAMPLE>
Thanks for visiting this category strategy page on Snippets in GitLab. This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by <PM NAME>([E-Mail](mailto:<EMAIL@gitlab.com>) [Twitter](https://twitter.com/<TWITTER>)).

This strategy is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for snippets, we'd especially love to hear from you.
</EXAMPLE>
-->

👋 This is the category strategy for Issue Tracking in GitLab; which is part of the Plan stage's [Project Management](/handbook/categories/#project-management-group) group. Please reach out to the group's Product Manager, Gabe Weaver ([E-mail](mailto:gweaver@gitlab.com)), if you'd like to provide feedback or ask any questions related to this product category.

This strategy is a work in progress and everyone can contribute:

- Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aproject%20management) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AIssue%20Tracking) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
- Please share feedback directly via email, Twitter, or on a video call.

### Overview

<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->

#### Purpose

GitLab's mission is to build software so that **everyone can contribute**. Issues are the fundamental medium for enabling collaboration on ideas and tracking that idea as it turns into reality.

#### Essential Intent

The goal of a Category's "Essential Intent" is to provide a concrete, inspirational statement. Another way to think of it is answering this single question -- *"If Issue Tracking can be truly excellent at only one thing, what would it be?"* This is Issue Tracking's Essential Intent:

> To provide a **clear**, **single source of truth** for an idea from inception to implementation that the members of a community can **trust** and **effectively collaborate** on from anywhere within the GitLab application without breaking or unnecessarily delaying a member's current flow.

Next, asking "How will we know when we're done?" provides clarity of purpose. This is how we will know:

- Issues take less than 1 second to load and there is never more than a 1 second delay in propogating changes on an issue to any member; Members never interact with an issue that does not contain the most recent changes.
- Within 15-30 seconds of first seeing an issue above the fold, any community member can understand the purpose of the idea, that it is up to date and relevant, how it contributes to the wider community's purpose or goals, what the intrinsic value is, where it is in its lifecycle, what the next action is to move the idea forward, who owns that action, and that it sets a reasonable expectation regarding when the idea will be fully implemented that can be trusted with a high level of certainty. Issues that do not satisfy this criteria should not be trusted. Communities never have more than 1% of Issues that are untrusted and no single issue remains untrusted for more than 5 days. We do not rely on humans to determine whether an issue can be trusted or not, but humans are responsible for maintaing trust.
- Issues leverage [autonomation](https://en.wikipedia.org/wiki/Autonomation) to empathetically encourage wider community members to improve the quality of the issue, remove many manual processes involved with issue management, and help community members focus on contributing to the top 10% of ideas that will yield 90% of the total value for the beneficiaries of the issue.
- New ideas are always welcome from anyone (whether an end user, team member, or internal stakeholder), but they are never duplicated. This helps focus the diverse perspective from the wider community around a singular idea to yield the best possible outcome. We do not rely on humans to prevent duplication.
- Issues should be organized in a way that is easily discoverable and accessible to the community the issue serves; but also in a way that optimizes an individual's ability to interact with it based on their personal workflows and how they work best.
- Every single change to issues, including the configuration of data properties present on issues such as label and milestone management, are transparent, traceable, auditable, and easily reversible.

#### How we prioritize

We use the following decision framework when evaluating what to work on. Each idea must meet all three of the minimum criteria and two of the extreme criteria.

|                  | Criteria 1                          | Criteria 2                           | Criteria 3                    |
| ---------------- | ----------------------------------- | ------------------------------------ | ----------------------------- |
| Minimum (3 of 3) | simplifies collaboration            | reduces the friction to contributing | aligned with market direction |
| Extreme (2 of 3) | enhances the ability to build trust | decreases manual effort              | increases clarity and purpose |

#### Target Audience

<!--
List the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->

As issues are one of the primary, fundamental mediums for collaborating on ideas within GitLab, every single one of our internal and external [personas](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#user-personas) falls within Issue Tracking's traget audience.

#### Challenges to address

<!--
- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->

- 60% of enterprise software projects have overuns.
- The market is littered with fragmented, overly complicated issue tracking and project management tools that are [slow, cumbersome](http://log.liminastudio.com/writing/commentary/why-i-finally-ditched-jira), and prioritize enforced workflows over facilitating meaningful collaboration. Most are [not organized](https://news.ycombinator.com/item?id=18985487) around how teams actually prefer to plan and track work; and require advanced configuration and numerous integrations with third-party services before they can provide the level of traceability and compliance that enterprises require.
- GitLab is also [contributing to this problem](https://news.ycombinator.com/item?id=14353746).
- Organizations struggle to quantify the value of their work; leading to prioritization by the "loudest voice in the room" as opposed to business results and customer value.

### Where we are Headed

<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized -->

We've written a [mock press release](/direction/plan/project_management/one_year_plan.html) describing where we intend to be by 2020-09-01. We will maintain this and update it as we sense and respond to our customers and the wider community.

#### What's Next & Why

<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-management/process/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

These are the epics we will be working on over the next few releases:

- [Manage sprint and release cadence](https://gitlab.com/groups/gitlab-org/-/epics/69)
  - **Outcome:** Teams can track work within the timeboxed sprint/release cadence common with popular methodologies such as Scrum and ExtremeProgramming.
- [Track scope change within milestone](https://gitlab.com/groups/gitlab-org/-/epics/1957)
  - **Outcome:** Teams are able to track scope changes throughout a milestone and review a milestone summary report that surfaces information such as scope added, scope removed, and issues that missed the milestone.
- [Issue Task Lists and Tasks](https://gitlab.com/groups/gitlab-org/-/epics/543)
  - **Outcome:** Teams can collaborate on a single issue while still being able to track function specific weights. This is part of the broader vision for capacity management within GitLab.
- [Team velocity and volatility](https://gitlab.com/groups/gitlab-org/-/epics/435)
  - **Outcome:** Teams are able to set better delivery committments and increase the speed at which they improve processes.
- [Custom Fields](https://gitlab.com/groups/gitlab-org/-/epics/235)
  - **Outcome:** Teams can track additional meta data on issues without cluttering up the core issue experience.

Our group level [issue board](https://gitlab.com/groups/gitlab-org/-/boards/1235826?&label_name[]=devops%3A%3Aplan&label_name[]=group%3A%3Aproject%20management) provides detailed insight into everything currently in flight.

#### What is Not Planned Right Now

<!-- Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

- Integrating machine learning and natural language processing. While this will be important in the coming years, we need to provide a base level of support for modern agile teams before introducing optimizations.
- Sub-issues in the traditional sense. Based on our understanding of how they are used in other project management products, we feel like there is a [better approach](https://gitlab.com/gitlab-org/gitlab/issues/2036#note_213039321) that will require less issue management overhead.

#### Maturity Plan

<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

This category is currently at the 😁**Complete** maturity level, and our next maturity target is 😍**Lovable** by 2020-09-01.

We are tracking our progress against this target via [this epic](https://gitlab.com/groups/gitlab-org/-/epics/1933).

### User success metrics

<!--
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->

We are currently using the [loose Stage Monthly Active Users (SMAU) definition](/handbook/product/categories/plan/#metrics) and intend on migrating to the strict definition as soon as we've implemented the necessary telemtry to measure the defined events.

### Why is this important?

<!--
- Why is GitLab building this feature?
- Why impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->

Issues are the fundamental medium for enabling collaboration on ideas and tracking that idea as it turns into reality. Over 1 million new issues are created per month and over 200k are interacted with on a daily basis across all instances of GitLab.

Improving collaboration on issues and how they are tracked directly correlates to a decrease in overall cycle time for a given change within a team's DevOps workflow.

### Competitive Landscape

<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-management/process/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

The [project management tools market](https://www.datanyze.com/market-share/project-management) is dominated by Jira (31%), Microsoft (18%), Smartsheet (6%), and Trello (5%). To be competitive, GitLab needs to solve for the following customer comments in the immediate future:

> We’ve been told to use gitlab’s milestones to capture agile sprints, as they’re the only thing that can have burn down, they work well with boards and have concepts of time.  The default way epics get their start/end dates is based on the milestones of the issues attached though – doesn’t make sense as an issue shouldn’t be assigned to an agile sprint until the sprint is eminent.  (root of problem is that gitlab milestone must either be equated to agile milestone or agile sprint, not both – you’re missing a concept)
>
> Needs to be a way to have a team velocity, as a scrum master be able to go through and say “This feature requires ~100 points of work, we can do 25 points per sprint, will take 4 sprints (8 weeks) – you want it done in 6 weeks, will either require to be simplified or increased resourcing.”
>
> Need burn-down chart/progress status of sprints, features, initiatives, and milestones.
>
> During sprint planning, need a way to see what my team’s velocity has been last several sprints to have a good idea of how much we should be planning for upcoming sprint.
>
> Need an easy way to see how much I’m assigning to each team member during sprint planning (team members aren’t interchangeable – sprint can have user stories less than velocity, but if user stories are only doable by one team member then the work can’t get done).
>
> Need to be able to answer questions around “which teams/members are working on this feature?”, “are we still on track to meet this milestone?”, “we want to add this new feature, how will that slow down other development?”, “This team is needed for another project, how will that effect timelines on this project?”, etc...

### Analyst Landscape

<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

What they are saying:

- Within the next few years, over 2/3 of organisations will adopt a product-centric delivery model and move away from project management practices.
- "Agile" adoption remains strong and shows no signs of slowing down.
- Successful project/product management solutions must function as part of the overall DevOps toolchain and serve as a hub for planning and measuring value delivery.
- Competitive products should also provide feedback in the form of qualitative and quantitative metrics such as customer value and satisfaction, deliver velocity and volatility, business outcomes, cycle times, and organizational effectiveness around adopting modern DevOps and "Agile" practices.

### Top Customer Success/Sales issue(s)

<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

- [Overview of Issue Boards across ALL projects](https://gitlab.com/gitlab-org/gitlab/issues/15757) (👍 99, +4)
- [Provide a way to work with a referenced issue from anywhere in the product without leaving your current context](https://gitlab.com/gitlab-org/gitlab/issues/13319) (👍 5, +3)
- [Manage sprint and release cadence](https://gitlab.com/groups/gitlab-org/-/epics/69) (👍 3, +2)

### Top user issue(s)

<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

- [List of all subscribed issues](https://gitlab.com/gitlab-org/gitlab/issues/14972) (👍 207, +18)
- [Trello Powerup: Add ability to attach issues](https://gitlab.com/gitlab-org/trello-power-up/issues/31) (👍 241, +9)
- [Desktop Push Web Notifications](https://gitlab.com/gitlab-org/gitlab/issues/17232) (👍 167, +12) and an overhaul of how we handle notifications in general via something like a [Unified Notifications Center](https://gitlab.com/gitlab-org/gitlab/issues/14889) (👍 91, +7)
- [Access issue custom fields from project level](https://gitlab.com/gitlab-org/gitlab/issues/1906) (👍 147, +13)
- [Remove list labels when closing an issue](https://gitlab.com/gitlab-org/gitlab/issues/17461) (👍 133, +12)
- [Clone issue with quick action](https://gitlab.com/gitlab-org/gitlab/issues/9421) (👍 116, +5)
- [Group level description templates for issues and merge requests](https://gitlab.com/gitlab-org/gitlab/issues/7749) (👍 127, +16)
- [Add control over images in Markdown](https://gitlab.com/gitlab-org/gitlab/issues/15697) (👍 146, +2)
- [Add branch references to GFM](https://gitlab.com/gitlab-org/gitlab/issues/17084) (👍 113, +1)

### Top internal customer issue(s)

<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

- [Add an existing branch to a specific issue](https://gitlab.com/gitlab-org/gitlab/issues/15901) (👍 110, +4)
- [Attachment manager](https://gitlab.com/gitlab-org/gitlab/issues/16229) (👍 99, +2)
- [Provide first class support for Issue Task Lists and Tasks](https://gitlab.com/gitlab-org/gitlab/issues/2036) (👍 56, +7)
- [Multiple milestones per issue or merge request](https://gitlab.com/gitlab-org/gitlab/issues/5135) (👍 51, +15)
- [More historically accurate burndown charts](https://gitlab.com/gitlab-org/gitlab/issues/6903) (👍 12, +4)

### Top Strategy Item(s)

<!-- What's the most important thing to move your strategy forward?-->

One of our one year goals is to provide first-class support for modern "Agile" methodologies. Here are the next most important items to that end:

- Refactor how we handle Milestones. Right now, Milestones are largely stateless so it's impossible to calculate a velocity from Milestone to Milestone as we are retaining very little historical information in Milestones.
- Introduce some concept of a team so we can begin to measure and track capacity.
- Support [multiple milestones](https://gitlab.com/gitlab-org/gitlab/issues/5135) per Issue or something similar that enables an issue to be associated to be a release and a sprint.
- Improve the integration of reports, milestones, and epics within Issue Boards.

Another goal is to make it easier to collaborate while staying in flow. In order to enable users to interact with Issues from anywhere within the application, we will need to complete some fairly massive refactoring to support soft-real time functionality. These are the next most important steps:

- Add support for [websockets](https://gitlab.com/gitlab-org/gitlab/issues/21249).
- Complete our implementation of GraphQL and refactor the frontend to use GraphQL /Subscriptions.
